// console.log("Hello world");

// What are conditional statements?
// Conditional statements allow us to control the flow of our program and it allow us to run statement/instruction based on the condition.

// if statement
// Executes a statement if a specified condition is true

/* 
	Syntax: 
		if(condition){
			code block /statements;
		}

*/

let numA = 0;
if (numA < 0){
	console.log("Hello");
}

// The codeblock inside the if statement will not work if it doesn't satisfy the condition 

if(numA == 0){
	console.log("Hello");
}

// The result of the expression must result to "true", else will not run statement inside 

console.log(numA < 0); //false
console.log(numA == 0); // true


let city = "New York";

if (city=="New York"){
	console.log("Welcome to New York City");
}


//  else if clause 
/*
	Executes a statement if previous condition/s are false and if the specified condition is true
	The "else if" clase is optional and can be added to capture additional conditions to change the flow of the program.


*/

if(city === "New York"){
	console.log("Welcome to New York City!");
}

else if (city === "Toko"){
	console.log("Welcome to Tokyo, Japan");
}

else {
	console.log("City is not included in the list.");
}



console.log("----------------");




let numH = 0;

if (numH < 0){ // retured false
	console.log("The number is negative");
}

else if(numH > 0){ // return false 
	console.log("The number is positive");
}
else if(numH == 0){
	console.log("The number is zero");
}





console.log("----------------");



//  Alternative code 
if (numH < 0){ // retured false
	console.log("The number is negative");
}

else if(numH > 0){ // return false 
	console.log("World");
}
else {
	console.log("The value is zero");
}


// else statement 
	
/*

	Executes the statement if all other condition are false.
	The 'else' statement is optional and can be added to capture any other result to change the flow of the program.

*/


 // -----------------------------------	

 let message = "No message";
	console.log(message);

	function determineTyphooneIntensity(windSpeed){

		if(windSpeed < 30){
			return "Not a typhoon yet."
		}
		else if(windSpeed <= 61){
			return "Tropical depression detected."
		}
		else if (windSpeed >= 62 && windSpeed <= 88){
			return "Tropical storm detected."
		}
		else if(windSpeed >= 89 && windSpeed <= 117){
			return "Severe Tropical storm deteceted";
		}
		else{
			return "Typhoon detected."
		}
	}

	message = determineTyphooneIntensity(110);
	console.log(message);

	//  console.warn() is a good way to print warnings in our console that could help us developers act on certain output within our code.
	if(message === "Severe Tropical storm deteceted"){
		console.warn(message);
	}


// [SECTION] Truthy and False 

/* 
	- In JS a "truthy" value that is considered true when encountered in a boolean context

	- False 
		1. false 
		2. 0
		3. ""
		4. null
		5. undefined // let number; 
		6. NaN (Not a number)

*/
let isMarried = true

//  Truty examples:
if (true) {
	console.log("Truthy");
}
if (1){
	console.log("Truthy");
}
if ([]) {
	console.log("Truthy");
}

// Falsy examples 
if (false){
	console.log("Falsy");
}
if (0){
	console.log("Falsy");
}
if (undefined){
	console.log("Falsy");
}


if(isMarried){
	console.log("Truthy");
}


//--------------------------------

// [SECTION] Conditional (ternary) Operator

/* 
	The Conditional (Ternady) operator takes in three operands
*/


let ternaryResult = (1 < 18)? true:false;
	console.log("Result of Ternary Operator" + ternaryResult);





//  Multiple statement execution using ternary operator 


/*
let name;

function isOfLegalAge(){
	name="John";
	return "You are in the legal age";
}

function underAge(){
	name="Jane";
	return "You are under age limit";
}

// parseInt - converts the input received into a number dataype 
	// if the input is not a number , it will return NaN (Not a number)
let age = parseInt(prompt("What is your age"));
console.log(age);

						   // true         // false
let legalAge = (age>=18)? isOfLegalAge() : underAge();

console.log("Return of Ternary Operator in Functions " + legalAge + " , " + name);
*/





// [SECTION] Switch Statements



/*

let day = prompt("What day of the weeks is it today").toLowerCase();  // .toLowerCase - to make input lowercase regardless 
	console.log(day); // display input

switch(day){
	case "monday":
		console.log("The color of the day is Red"); // display output
		break; // so that it will no longer check the next case 
	case "tuesday":
		console.log("The color of the day is Orange");
		break; 
	case "wednesday":
		console.log("The color of the day is Yellow");
		break; 
	case "thursday":
		console.log("The color of the day is Green");
		break; 
	case "friday":
		console.log("The color of the day is Blue");
		break; 
	case "saturday":
		console.log("The color of the day is Indigo");
		break; 
	case "sunday":
		console.log("The color of the day is Violet");
		break; 
	default:
	 	console.log("Please input a valid day");
	 	break; 
}

*/





// [SECTION] Try-Catch-Finally Statement

let codeErrorMessage = "Code Error";
							// parameter
function showIntensityAlert(windSpeed){
	try{
		// codes/code block to try 
		alerat(determineTyphooneIntensity(windSpeed));

	}
	catch(error){
		// error.message is used to access info relating to an error object
		console.warn(error.message);
	}
	finally{
		alert("Intensity updates will show new alert.");
	}
}	


showIntensityAlert(56);


console.log("Sample output after try-catch-finally");





















